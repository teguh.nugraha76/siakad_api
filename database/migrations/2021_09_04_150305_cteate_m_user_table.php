<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CteateMUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_user', function (Blueprint $table) {
            $table->increments('id_user', 4);
            $table->string('id_user_type', 2)->nullable();
            $table->string('name', 255)->nullable();
            $table->string('pob', 50)->nullable();
            $table->string('dob', 25)->nullable();
            $table->string('id_province', 10)->nullable();
            $table->string('id_city', 10)->nullable();
            $table->string('id_district', 10)->nullable();
            $table->string('id_village', 10)->nullable();
            $table->string('education', 150)->nullable();
            $table->string('email', 150)->nullable();
            $table->string('phone', 20)->nullable();
            $table->string('created_at', 25)->nullable()->useCurrent();
            $table->string('created_by', 25)->nullable();
            $table->string('updated_at', 25)->nullable()->useCurrent();
            $table->string('updated_by', 25)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_user');
    }
}
