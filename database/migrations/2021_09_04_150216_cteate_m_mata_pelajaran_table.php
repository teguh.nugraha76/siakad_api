<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CteateMMataPelajaranTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_mata_pelajaran', function (Blueprint $table) {
            $table->unsignedInteger('id_mata_pelajaran');
            $table->primary('id_mata_pelajaran');
            $table->string('name', 255)->nullable();
            $table->string('description', 255)->nullable();
            $table->tinyInteger('status')->nullable();
            $table->string('created_at', 25)->nullable()->useCurrent();
            $table->string('created_by', 25)->nullable();
            $table->string('updated_at', 25)->nullable()->useCurrent();
            $table->string('updated_by', 25)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_mata_pelajaran');
    }
}
