<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CteateMRuanganTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_ruangan', function (Blueprint $table) {
            $table->unsignedInteger('id_ruangan');
            $table->primary('id_ruangan');
            $table->string('name', 255)->nullable();
            $table->string('description', 255)->nullable();
            $table->tinyInteger('status')->nullable();
            $table->string('created_at', 25)->nullable()->useCurrent();
            $table->string('created_by', 25)->nullable();
            $table->string('updated_at', 25)->nullable()->useCurrent();
            $table->string('updated_by', 25)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_ruangan');
    }
}
