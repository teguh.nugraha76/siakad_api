<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class pembagian_kelas extends Model
{
    use HasFactory;
    protected $table = 'pembagian_kelas';
    protected $fillable = [
        'id_pembagian_kelas',
        'id_kelas',
        'id_teacher',
        'id_student',
        'status',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    ];

    function kelas()
    {
        return $this->hasOne(kelas::class, 'id_kelas', 'id_kelas');
    }

    function teacher()
    {
        return $this->hasOne(users::class, 'id_user', 'id_teacher');
    }

    function student()
    {
        return $this->hasOne(users::class, 'id_user', 'id_student');
    }
}
