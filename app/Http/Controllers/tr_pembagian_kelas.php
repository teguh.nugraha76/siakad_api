<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Helper\Helper;

use App\Models\pembagian_kelas;

class tr_pembagian_kelas extends Controller
{
    protected $data_req = [
        'name' => 'required'
    ];

    public function index(Request $request)
    {
        try {
            return response()->json([
                'status'  => 200,
                'message' => 'success',
                'data'    => Helper::queryParam($request->all(), 'App\Models\pembagian_kelas')
                    ->orderBy('updated_at', 'DESC')
                    ->with('kelas')
                    ->with('teacher')
                    ->with('student')
                    ->get()
            ], 200);
        } catch (\Exception $error) {

            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function detail($id_pembagian_kelas)
    {
        try {

            return response()->json([
                'status'  => 200,
                'message' => 'success',
                'data'    => pembagian_kelas::where('id_pembagian_kelas', $id_pembagian_kelas)
                    ->with('kelas')
                    ->with('teacher')
                    ->with('student')
                    ->first()
            ], 200);
        } catch (\Exception $error) {

            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function create(Request $request)
    {
        try {

            $input = $request->except(['created_at', 'created_by', 'updated_at', 'updated_by']);
            $data_post = [];

            foreach ($input['student'] as $key => $value) {
                $data = [
                    'id_kelas' => $input['id_kelas'],
                    'id_teacher' => $input['id_teacher'],
                    'id_student' => $value['id_student'],
                    'status' => $value['status'],
                ];

                $data_post[] = pembagian_kelas::create(array_merge($data, ['created_by' => $request->user()->username]));
            }

            return response()->json([
                'status'  => 200,
                'message' => 'success',
                'data'    => $data_post
            ], 200);
        } catch (\Exception $error) {

            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function delete_student()
    {
        try {

            return response()->json([
                'status'  => 200,
                'message' => 'success'
            ], 200);
        } catch (\Exception $error) {

            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }
}
