<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Http\Controllers\Controller;
use App\Helper\Helper;

use App\Models\users;

class UserController extends Controller
{
    protected $data_req = [
        'name' => 'required'
    ];

    public function index(Request $request)
    {
        try {
            return response()->json([
                'status'  => 200,
                'message' => 'success',
                'data'    => Helper::queryParam($request->all(), 'App\Models\users')->orderBy('updated_at', 'DESC')->with('user_type')->get()
            ], 200);
        } catch (\Exception $error) {

            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function detail($id_user)
    {
        try {

            return response()->json([
                'status'  => 200,
                'message' => 'success',
                'data'    => users::where('id_user', $id_user)->first()
            ], 200);
        } catch (\Exception $error) {

            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function create(Request $request)
    {
        try {

            $input = $request->except(['created_at', 'created_by', 'updated_at', 'updated_by']);

            /** Validate input */
            $validator = Validator::make($request->all(), $this->data_req);

            /** chack validate input */
            if ($validator->fails()) {
                return response()->json([
                    'status'    => 501,
                    'errorText' => 'Validation failed',
                    'message'   => $validator->errors()
                ], 501);
            }

            // return users::orderBy('id_user', 'desc')->first()->id_user + 1;

            users::create([array_merge($input, [
                'created_by' => $request->user()->username,
                'id_user' => users::orderBy('id_user', 'desc')->first()->id_user + 1,
            ])]);

            return response()->json([
                'status'  => 200,
                'message' => 'success',
                'data'    => $request->all()
            ], 200);
        } catch (\Exception $error) {

            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function update($id_user, Request $request)
    {
        try {

            $input = $request->except(['created_at', 'created_by', 'updated_at', 'updated_by']);

            /** Validate input */
            $validator = Validator::make($request->all(), $this->data_req);

            /** chack validate input */
            if ($validator->fails()) {
                return response()->json([
                    'status'    => 501,
                    'errorText' => 'Validation failed',
                    'message'   => $validator->errors()
                ], 501);
            }

            users::where('id_user', $id_user)->update(array_merge($input, ['updated_by' => $request->user()->username]));

            return response()->json([
                'status'  => 200,
                'message' => 'success',
                'data'    => $request->all()
            ], 200);
        } catch (\Exception $error) {

            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function delete($id_user)
    {
        try {
            $data = users::where('id_user', $id_user)->first();
            users::where('id_user', $id_user)->delete();

            return response()->json([
                'status'  => $data ? 200 : 500,
                'message' => $data ? 'delete success' : 'delete failed',
                'data'    => $data
            ], $data ? 200 : 500);
        } catch (\Exception $error) {

            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }
}
